import Vue from 'vue'
import Router from 'vue-router'
import HelloWorld from '@/components/HelloWorld'

Vue.use(Router)

export default new Router({
  routes: [
    {
      path: '/',
      component: () => import('../components/test/test.vue'),
    },
    {
      path: '/test1',
      component: () => import('../components/test/test1.vue'),
    },
    {
      path: '/h',
      name: 'HelloWorld',
      component: HelloWorld
    },
    {
      path: '/1',
      component: () => import('../components/test/1.vue'),
    },
    {
      path: '/3',
      component: () => import('../components/test/3.vue'),
    },
   
    {
      path: '/2',
      component: () => import('../components/test/2.vue'),
    }
  ]
})
