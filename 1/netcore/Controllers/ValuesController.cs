﻿using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using WebApplication1.Ef;

namespace WebApplication1.Controllers
{
    [Route("api/[controller]/[action]")]
    [ApiController]
    public class ValuesController : ControllerBase
    {
        [HttpGet]
        public ActionResult<string> Get() { return "1111"; }

     

        [HttpGet]
        public FileResult GetProse()
        {
            string xml = "";
            string path = Path.Combine(Directory.GetCurrentDirectory(), "prose");
            string filepath = Path.Combine(path, "1.txt");
            if (System.IO.File.Exists(filepath))
            {
                using (StreamReader sr = new StreamReader(filepath))
                {
                    xml = sr.ReadToEnd();
                }
            }
            byte[] content = System.Text.Encoding.UTF8.GetBytes(xml);
            return File(content, "text/plain");
        }

        [HttpGet]
        public JsonResult GetProseFromDb()
        {
            using (var context = new Context())
            {
                IQueryable<right> rights = context.rights;
                return new JsonResult(  Newtonsoft.Json.JsonConvert.SerializeObject(rights));
            }
        }

     


        // DELETE api/values/5
        [HttpPost]
        public void Delete(Dictionary<string, string> title)
        {
            using (var context = new Context())
            {
                IQueryable<right> rights = context.rights;
                right rightone = rights.Where(m => m.title == title["title"]).FirstOrDefault();
                if (rightone != null)
                {
                    context.rights.Remove(rightone);
                    context.SaveChanges();
                }
            }
        }

        [HttpPost]
        public string  Add([FromBody] Dictionary<string, string> title)
        {
            using (var context = new Context())
            {
                right rightone = new right();
                var jsonSerializer = new JsonSerializer();
                rightone.title = title["title"] ;
                context.rights.Add(rightone);
                context.SaveChanges();
                return "true";


            }
           
        }


    }
}