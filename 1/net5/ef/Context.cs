﻿using Microsoft.EntityFrameworkCore;

namespace WebApplication1.Ef
{
    public class Context : DbContext
    {
        public Context()
            : base()
        {
        }

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            optionsBuilder.UseSqlServer("Server=.;Database=test;User Id=sa;Password=111111;Integrated Security=SSPI");
            base.OnConfiguring(optionsBuilder);
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<right>().ToTable("right");

            base.OnModelCreating(modelBuilder);
        }

        public DbSet<right> rights { get; set; }
    }
}